﻿#pragma strict
//internal var startTime;
var fireBall : Rigidbody;
var rockBall1 : Rigidbody;
var rockBall2 : Rigidbody;
var rockBall3 : Rigidbody;
var rockBalls = [rockBall1, rockBall2, rockBall3];
var snowBall1 : Rigidbody;

var projectileLifespan: float  = 10;

var speed = 5.0;
var offset = new Vector3(0,0,0);
function Start () {
	fireBall.GetComponent(shot).setLifespan(-1);
	rockBall1.GetComponent(shot).setLifespan(-1);
	rockBall2.GetComponent(shot).setLifespan(-1);
	rockBall3.GetComponent(shot).setLifespan(-1);
	snowBall1.GetComponent(shot).setLifespan(-1);
}

function FireProjectile (projectile : Rigidbody) {
	
    projectile.velocity  = GameObject.Find("CameraLeft").gameObject.transform.forward * speed;
    projectile.GetComponent(shot).setLifespan(projectileLifespan);
    // You can also acccess other components / scripts of the clone
    //rocketClone.GetComponent(MyRocketScript).DoSomething();
}

function Update () {
	//print(transform.position + offset);
	if (Input.GetKeyDown("z")) {
		
		transform.position = Vector3(transform.position.x, 1.6, transform.position.z);
		print(transform.position + offset);
		var fireClone : Rigidbody = Instantiate(fireBall, transform.position, transform.rotation);
		FireProjectile(fireClone);
	}
	else if (Input.GetKeyDown("x")){
		transform.position = Vector3(transform.position.x, 1.6, transform.position.z);
		var randRock : int = Mathf.Floor(Random.value * rockBalls.length);
		var rockAngleVelocity : Vector3 = Vector3 (Random.value*100, Random.value*100, Random.value*100);
		var rockRotation : Quaternion = Quaternion.Euler(rockAngleVelocity);
		var rockClone : Rigidbody = Instantiate(rockBalls[randRock], transform.position, rockRotation);
		FireProjectile(rockClone);
	}
	
	else if (Input.GetKeyDown("c")){
		transform.position = Vector3(transform.position.x, 1.6, transform.position.z);
		var snowAngleVelocity : Vector3 = Vector3 (Random.value*50, Random.value*50, Random.value*50);
		var snowRotation : Quaternion = Quaternion.Euler(snowAngleVelocity);
		var snowClone : Rigidbody = Instantiate(snowBall1, transform.position, snowRotation);
		FireProjectile(snowClone);
	}
}